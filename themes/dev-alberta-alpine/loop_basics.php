		<!-- Beginning of loop_basics.php -->
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<!-- post thumbnail -->
		<div class="news_story_header">
			
			<?php if ( has_post_thumbnail()) { // Check if thumbnail exists, if yes you get the thumbnail and title, ELSE just the title ?>
				<div class="news_story_thumbnail col-xs-12 col-sm-3">
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
						<?php the_post_thumbnail(array(120,120)); // Declare pixel size you need inside the array ?>
					</a>
				</div>
				<div class="news_story_headline col-xs-12 col-sm-9">
				<!-- post title -->
				<h2>
					<a class="sidebar_foreground" href="<?php the_permalink(); ?>" title="c"><?php the_title(); ?></a>
				</h2>
				<span class="date"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span><br/>
				
				
				<!-- /post title -->
					<!-- post details -->
				
				<span class="comments"><?php if (comments_open( get_the_ID() ) ) comments_popup_link( __( 'Leave your thoughts', 'html5blank' ), __( '1 Comment', 'html5blank' ), __( '% Comments', 'html5blank' )); ?></span><br/>
				<?php edit_post_link(); ?><br/>
				<!-- /post details -->
			</div>
			<?php } else { ?>
			
			<!-- /post thumbnail -->
			<div class="news_story_headline">
				<!-- post title -->
				<h2>
					<a class="sidebar_foreground" href="<?php the_permalink(); ?>" title="c"><?php the_title(); ?></a>
				</h2>
				<span class="date"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span><br/>
				
				
				<!-- /post title -->
					<!-- post details -->
				
				<span class="comments"><?php if (comments_open( get_the_ID() ) ) comments_popup_link( __( 'Leave your thoughts', 'html5blank' ), __( '1 Comment', 'html5blank' ), __( '% Comments', 'html5blank' )); ?></span><br/>
				<?php edit_post_link(); ?><br/>
				<!-- /post details -->
			</div>
			<?php } ?>
			
		</div>
		<div class="news_story_content">
		

			<?php the_excerpt(); ?>

			
		</div>
		
	</article>
	
	<hr/>
	<!-- /article -->
	<!-- end of loop_basics.php -->