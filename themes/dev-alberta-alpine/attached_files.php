<!-- attached_files.php -->
<?php if( have_rows('file_uploads') ): ?>
				<br/>Attached Files:
				<div class="page_content_meta">
			
				<?php while ( have_rows('file_uploads') ) : the_row(); ?>

					
					
						
						<?php 
						
						$file = get_sub_field('file_upload');

						
						echo '<div class="results_table">';
						

						if(get_sub_field('file_title'))
						{
							echo '<span class="results_cell"><img class="file_icon" src="'.$file["icon"].'" alt="icon" />'.the_sub_field('file_title');'</span>';
						} else { 
						echo '<a href="'. $file["url"] .'" target="_blank"><span class="results_cell"><img class="file_icon" src="'.$file["icon"].'" alt="icon" />'.$file["title"].'</span>';
						};
						echo '</a>';
						echo '';
						echo '<span class="results_cell">Uploaded: '.$file["date"] .'</span>';
						echo '</div>';
						echo '<hr/>';
						?>
									
						
					
				<?php endwhile; ?>
			
				</div>
			<?php else :

				// no rows found

			endif;

			?>
			
<!-- /attached_files.php -->