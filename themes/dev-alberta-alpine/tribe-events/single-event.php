<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$events_label_singular = tribe_get_event_label_singular();
$events_label_plural = tribe_get_event_label_plural();

$event_id = get_the_ID();

?>

<?php if (has_tag('results')){ ?>
	
					<br/><span class="main_background results_title"><h5>RESULTS:</h5></span>
					<span class="triangle_50px pull-left main_triangle">
					</span>
					
					<div class="page_content_meta">
				
				<?php include (TEMPLATEPATH . '/attached_files.php'); ?>
				
	<?php } else {

	}	?>

	

<div id="tribe-events-content" class="tribe-events-single vevent hentry">

	<p class="tribe-events-back">
		<a href="<?php echo esc_url( tribe_get_events_link() ); ?>"> <?php printf( __( '&laquo; All %s', 'tribe-events-calendar' ), $events_label_plural ); ?></a>
	</p>

	<!-- Notices -->
	<?php tribe_events_the_notices() ?>

	

	<div class="tribe-events-schedule updated published tribe-clearfix">
		<?php echo tribe_events_event_schedule_details( $event_id, '<h3>', '</h3>' ); ?>
		<?php if ( tribe_get_cost() ) : ?>
			<span class="tribe-events-divider">|</span>
			<span class="tribe-events-cost"><?php echo tribe_get_cost( null, true ) ?></span>
		<?php endif; ?>
	</div>

	<!-- Event header -->
	<div id="tribe-events-header" <?php tribe_events_the_header_attributes() ?>>
		<!-- Navigation -->
		<h3 class="tribe-events-visuallyhidden"><?php printf( __( '%s Navigation', 'tribe-events-calendar' ), $events_label_singular ); ?></h3>
		<ul class="tribe-events-sub-nav">
			<li class="tribe-events-nav-previous"><?php tribe_the_prev_event_link( '<span>&laquo;</span> %title%' ) ?></li>
			<li class="tribe-events-nav-next"><?php tribe_the_next_event_link( '%title% <span>&raquo;</span>' ) ?></li>
		</ul>
		<!-- .tribe-events-sub-nav -->
	</div>
	<!-- #tribe-events-header -->

	<?php while ( have_posts() ) :  the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<!-- Event featured image, but exclude link -->
			<?php echo tribe_event_featured_image( $event_id, 'full', false ); ?>

			<!-- Event content -->
			<?php do_action( 'tribe_events_single_event_before_the_content' ) ?>
			<div class="tribe-events-single-event-description tribe-events-content entry-content description">
				<?php the_content(); ?>
			</div>
			<!-- .tribe-events-single-event-description -->
			<div class="col-md-12">
			<?php do_action( 'tribe_events_single_event_after_the_content' ) ?>
			</div>
			<!-- Event meta -->
			<div class="col-md-12">
			<?php do_action( 'tribe_events_single_event_before_the_meta' ) ?>
			
			<?php
			/**
			 * The tribe_events_single_event_meta() function has been deprecated and has been
			 * left in place only to help customers with existing meta factory customizations
			 * to transition: if you are one of those users, please review the new meta templates
			 * and make the switch!
			 */
			if ( ! apply_filters( 'tribe_events_single_event_meta_legacy_mode', false ) ) {
				tribe_get_template_part( 'modules/meta' );
				

			} else {
				echo tribe_events_single_event_meta();
					
			}
		
			?>
			
			<?php do_action( 'tribe_events_single_event_after_the_meta' ) ?>
			</div>
		</div> <!-- #post-x -->
		<hr class="clearfix" />
		<?php if ( get_post_type() == Tribe__Events__Main::POSTTYPE && tribe_get_option( 'showComments', false ) ) comments_template() ?>
	<?php endwhile; ?>
	<?php if (has_tag('results')){

	} else { ?>
	<!-- Attached files -->
				<?php if( have_rows('file_uploads') ): ?>
					<br/>Attached Files:
					<div class="page_content_meta">
				
					<?php while ( have_rows('file_uploads') ) : the_row(); ?>

						<div class="file_upload col-xs-6 col-md-4">	
						
							
							<?php 

							$file = get_sub_field('file_upload');

							
							echo '<a href="'. $file["url"] .'" target="_blank">';
							echo '<img src="'.$file["icon"].'" alt="icon" /><br/>';

							if(get_sub_field('file_title'))
							{
								echo '<span>'.the_sub_field('file_title');'</span>';
							} else { 
							echo '<span>'.$file["title"].'</span>';
							};
							echo '</a>';
							echo '<br/>';
							echo 'Uploaded: '.$file["date"];
							
							?>
										
							
						</div>
					<?php endwhile; ?>
				
					</div>
				<?php else :

					// no rows found

				endif;

				?>
	<?php } ?>
	<!-- Event footer -->
	<div id="tribe-events-footer">
		<!-- Navigation -->
		<h3 class="tribe-events-visuallyhidden"><?php printf( __( '%s Navigation', 'tribe-events-calendar' ), $events_label_singular ); ?></h3>
		<ul class="tribe-events-sub-nav">
			<li class="tribe-events-nav-previous"><?php tribe_the_prev_event_link( '<span>&laquo;</span> %title%' ) ?></li>
			<li class="tribe-events-nav-next"><?php tribe_the_next_event_link( '%title% <span>&raquo;</span>' ) ?></li>
		</ul>
		<!-- .tribe-events-sub-nav -->
	</div>
	<!-- #tribe-events-footer -->

</div><!-- #tribe-events-content -->
