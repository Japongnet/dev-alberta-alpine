<?php 
/**
 * The Template for displaying all Single Profile posts.
 *
 * @package WordPress

 */




get_header(); ?>
	<!-- Beginning of single-profile.php -->
	<main role="main">
	<!-- section -->
	<section>
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div class="col-xs-12">
				<h1><?php the_field('profile_name'); ?></h1>
				
					<div class="col-xs-6">
					<?php if ( get_field('profile_image') ){ ?>

						<img class="profile_image" src="<?php the_field('profile_image'); ?>" alt="profile picture" /> 
						<img src="<?php get_field('profile_image') ?>" />
					<?php } else { ?>
						<img class="profile_image" src="<?php the_field('gallery_image_placeholder', 35) ?>" alt="profile picture" /> 
						
					<?php }; ?>
					</div>
					<div class="col-xs-6 profile_details">
						<span class=""><?php the_field('profile_meta'); ?></span>
					</div>
				
				
			</div>
			
			<hr class="clearfix" />
			<div class="col-xs-12">
				<?php the_field('profile_bio'); ?>
			</div>
			<br/>
			
			<br/>
		
			
			
			<br/
			
			<!-- /post details -->

			
			

			

			

			<?php edit_post_link(); // Always handy to have Edit Post Links available ?>
			
			<!-- Attached files -->
			<?php if( have_rows('file_uploads') ): ?>
				<br/>Attached Files:
				<div class="page_content_meta">
			
				<?php while ( have_rows('file_uploads') ) : the_row(); ?>

					<div class="file_upload col-xs-6 col-md-4">	
					
						
						<?php 

						$file = get_sub_field('file_upload');

						
						echo '<a href="'. $file["url"] .'" target="_blank">';
						echo '<img src="'.$file["icon"].'" alt="icon" /><br/>';

						if(get_sub_field('file_title'))
						{
							echo '<span>'.the_sub_field('file_title');'</span>';
						} else { 
						echo '<span>'.$file["title"].'</span>';
						};
						echo '</a>';
						echo '<br/>';
						echo 'Uploaded: '.$file["date"];
						
						?>
									
						
					</div>
				<?php endwhile; ?>
			
				</div>
			<?php else :

				// no rows found

			endif;

			?>

		</article>
		
		
		<!-- /article -->

	<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<article>

			<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>

		</article>
		<!-- /article -->

	<?php endif; ?>
	
	

	</section>
	<!-- /section -->
	</main>

<?php get_sidebar(); ?>
<!-- end of single.php -->
<?php get_footer(); ?>
