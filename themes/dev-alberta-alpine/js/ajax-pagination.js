(function($) {
 
	function find_page_number( element ) {
		element.find('span').remove();
		return parseInt( element.html() );
	}
	 
	$(document).on( 'click', '.page-numbers', function( event ) {
		event.preventDefault();
		 
		page = find_page_number( $(this).clone() );
		 
		$.ajax({
			url: ajaxpagination.ajaxurl,
			type: 'post',
			data: {
				action: 'ajax_pagination',
				query_vars: ajaxpagination.query_vars,
				page: page
			},
			success: function( html ) {
				$('.main').find( 'article' ).remove();
				$('.main .pagination').remove();
				$('.main hr').remove();
				$('.main').append( '<div class="ajax_loading">Loading Posts...</div>' );
				$('.main').append( html );
				$('.main article').hide().fadeIn(250);
				$('.main .ajax_loading ').fadeOut(250);
			}
		})
	})
})(jQuery);