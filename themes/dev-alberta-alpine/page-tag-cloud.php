<?php /* Template Name: Tag Cloud */ get_header(); ?>
	<!-- Beginning of page.php -->
	<main role="main">
		<!-- section -->
		<section>

			<h1><?php the_title(); ?></h1>

			<?php wp_tag_cloud( ); ?>

		</section>
		<!-- /section -->
	</main>

<?php get_sidebar(); ?>
<!-- end of page.php -->
<?php get_footer(); ?>
