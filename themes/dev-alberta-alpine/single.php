<?php get_header(); ?>
	<!-- Beginning of single.php -->
	<main role="main">
	<!-- section -->
	<section>

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<!-- post thumbnail -->
			<?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
				<div class="col-xs-12 col-sm-3">
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
					<?php the_post_thumbnail(); // Fullsize image for the single post ?>
				</a>
				</div>
				<div class="col-xs-12 col-sm-9">
					<h1>
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
					</h1>
				</div>
				<?php else: ?>
				<h1>
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
				</h1>
			<?php endif; ?>
			<!-- /post thumbnail -->

			<!-- post title -->

			<!-- /post title -->

			<!-- post details -->
			<br/>
			<hr class="clearfix" />
			<span class="date"><?php the_time('F j, Y'); ?></span><br/><br/>
		
			
			
			<!-- /post details -->

			<?php the_content(); // Dynamic Content ?>

			

			<p><?php _e( 'Categorised in: ', 'html5blank' ); the_category(', '); // Separated by commas ?></p>

			

			<?php edit_post_link(); // Always handy to have Edit Post Links available ?>
			
			<?php include (TEMPLATEPATH . '/attached_files.php'); ?>
			
		
			

		</article>
		
		
		<!-- /article -->

	<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<article>

			<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>

		</article>
		<!-- /article -->

	<?php endif; ?>
	
	

	</section>
	<!-- /section -->
	</main>

<?php get_sidebar(); ?>
<!-- end of single.php -->
<?php get_footer(); ?>
