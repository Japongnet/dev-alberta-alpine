	<!-- Beginning of Footer.php -->
			</section> <!-- Begins in header -->
		<!-- /section -->
		</div>
		<div class="col-xs-12  col-md-3 visible-xs-* hidden-sm hidden-md hidden-lg block-section  mid-section">
				
				
		</div>
		<div class="col-xs-12 col-sm-3 block-section mid-section">
			<?php get_sidebar( 'right' ); ?>
		</div>
	</div> <!--end of mid-wrapper -->	
	<div class="footer_wrapper col-xs-12 nopadding">
		<div class="col-xs-12 col-sm-6">
			<div class="footer_archive">
				<a href="<?php the_field( 'archive_content_link', 35 ); ?>"><div class="footer_archive_title">
					<h3 class="slab"><?php the_field( 'archive_content_title', 35 ); ?></h3>
				</div></a>
				<div class="footer_archive_triangle">
				</div>
				<div class="footer_archive_text">
					<h5><a href="<?php the_field( 'archive_content_link', 35 ); ?>"><?php the_field( 'archive_content_text', 35 ); ?></a></h5>
				</div>
			</div>
			
			
		</div>
		<div class="col-xs-12 col-sm-6">
			<div class="footer_block">
				<div class="promo_slider">
					<?php
						query_posts( array( 'post_type' => 'ad' ) );
						if ( have_posts() ) : while ( have_posts() ) : the_post();
					?>
					<a class="promo_slide" href="<?php the_field('ad_link'); ?>" target="_blank"><div class="ad_slide_image bg_contain" style="background-image:url('<?php the_field('ad_image'); ?>')"></div>
					</a>
						
					<?php endwhile; endif; wp_reset_query(); ?>
					
				</div>
			</div>
		</div>
	<!-- copyright -->
	<div class="copyright col-xs-12">
		
		
		<span class="col-xs-12 col-sm-6">
		<img class="footer_logo logo-img hidden-xs" src="<?php echo get_template_directory_uri(); ?>/img/AA_logo.png" alt="Logo">
		Copyright <?php echo date('Y'); ?> Alberta Alpine.<br/> Bill Warren Training Centre,
		Suite 100,1995 Olympic Way<br/>
		Canmore, AB T1W 2T6
		</span>
		<span class="col-xs-12 col-sm-6">
			Tel: 403-609-4730<br/>
			Fax: 403-678-3644<br/>

			<a href="/about/contact-us/">Contact Us</a>
		</span>
		
	</div>
	<!-- /copyright -->
	</div>
	
	<div class="footer_container">
		<div class="footer_padder trans_white">
		</div>
		<div class="triangle">
		</div>
	</div>
			<!-- footer -->
	<div class="footer_gradient">
		<div class="footer_gradient_top">
	
			<footer class="footer col-xs-12" role="contentinfo">

				

			</footer>
			<!-- /footer -->
		</div>
		<div class="footer_gradient_bottom">
		</div>
	</div>
		</div>
		<!-- /wrapper -->

		<?php wp_footer(); ?>

		<!-- analytics -->
		<script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
		ga('send', 'pageview');
		</script>

		<script>
		
		</script>
	</body>
		<!-- End of Footer.php -->
</html>
