<!-- sidebar-right.php -->
<aside class="sidebar sidebar-right" role="complementary">
	
	<?php
	if ( is_home() ) {
	} else {
		
		

		// Gallery Images
		
		if( have_rows('image_gallery') ): ?>
			<div class="page_content_meta">
			<a href="<?php the_field('gallery_image_placeholder', 35); ?>" class="fancybox"  data-fancybox-group="<?php the_title();?>" title="<?php the_title();?> Gallery"> 
				<h3>Gallery</h3>
			</a>
			<?php while ( have_rows('image_gallery') ) : the_row(); ?>

			<div class="gallery_item">	
			<a href="<?php the_sub_field('gallery_image'); ?>" class="fancybox"  data-fancybox-group="<?php the_title();?>" title="<?php the_sub_field('gallery_caption'); ?>">
				<div class="post_gallery_image bg_contain" style="background-image:url('<?php the_sub_field('gallery_image'); ?>')">
				</div>
				<?php the_sub_field('gallery_caption'); ?>
			</a>
			</div>
			<?php endwhile; ?>
			
			</div>
		<?php else :

			// no rows found

		endif;

		?>
	
		<?php the_tags( __( '<a href="../../tag-cloud/"><h3>Tags</h3></a> ', 'html5blank' ), ', ', '<br/><br/>'); // Separated by commas with a line break at the end ?>
		
		
		
		<?php
		$posttags = get_the_tags();
		
		if ($posttags) {
		  foreach($posttags as $tag) {
			$count++;
			
			  
			  $tag_result = $tag_result.$tag->slug . ',';
		  }
		}
		
		?>
		
		
		<?php
		$posttags = get_the_tags();
		
		if ($posttags) { ?>
			<h3>Related Content:</h3><br/>
			
			<?php
			// The Query
			$the_query = new WP_Query( 'tag='. $tag_result );

			// The Loop
			if ( $the_query->have_posts() ) {
				
				while ( $the_query->have_posts() ) {
					$the_query->the_post();
					echo '<a href="'. get_the_permalink() .'"> '. get_the_title() . '</a><br/>';
				}
				
			} else {
				// no posts found
				echo 'No related content found.'; 
			}
			/* Restore original Post Data */
			wp_reset_postdata();
			echo '<hr/>';	
		}
		?>
		
		
		
	<?php } ?>
		
	<?php

// check if the repeater field has rows of data
if( have_rows('repeater_field_name') ):

 	// loop through the rows of data
    while ( have_rows('repeater_field_name') ) : the_row();

        // display a sub field value
        the_sub_field('sub_field_name');

    endwhile;

else :

    // no rows found

endif;

?>

	<?php if (get_field('gallery_image') ): ?>
	<div class="content_meta">
		
			Gallery Image
			<?php the_field('gallery_image'); ?>
		</div>
	</div>
	<hr/>
	<?php endif; ?>
	<div class="sidebar-widget">
		<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('right')) ?>
		
	</div>

</aside>
<!-- /sidebar-right.php -->