<div class="race_series_widget">
	<div class="widget_header">
		<div class="widget_title">
		<h3>RACE SERIES</h3>
		</div>
		<span class="triangle_50px pull-left sidebar_triangle">
		</span>
	</div>
	<br class="clearfix" />



	
	
<?php

// check if the repeater field has rows of data
if( have_rows('race_series', $acfw) ): ?>
	 <?php 
 	// loop through the rows of data
    while ( have_rows('race_series', $acfw) ) : the_row(); ?>
		<div class="race_series_event">
			
			
				<div class="race_series_icon bg_contain" style="background-image:url('<?php echo the_sub_field('race_series_icon'); ?>')">
				
				</div>
			<a href="<?php echo the_sub_field('race_series_link'); ?>">	
				<div class="race_series_title">
				<?php echo the_sub_field('race_series_title'); ?>
				</div>
			
			</a>
		</div>
		
    <?php endwhile;

else :

    // no rows found

endif;

?>
</div>