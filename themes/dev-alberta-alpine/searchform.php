<!-- search -->
<hr/>
<div>
<form class="search clearfix" method="get" action="<?php echo home_url(); ?>" role="search">
	<input class="search-input pull-left" type="search" name="s" placeholder="<?php _e( 'Search AASA', 'html5blank' ); ?>">
	<span class="search_form_triangle pull-left">
	</span>
	
	<button class="search-submit" type="submit" role="button"><i class="fa fa-search"></i></button>
	
</form>
</div>
<hr/>
<!-- /search -->
