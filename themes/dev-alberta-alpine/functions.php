<?php
/*
 *  Author: Todd Motto | @toddmotto
 *  URL: html5blank.com | @html5blank
 *  Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

if (!isset($content_width))
{
    $content_width = 900;
}

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');

    // Add Support for Custom Backgrounds - Uncomment below if you're going to use
    /*add_theme_support('custom-background', array(
	'default-color' => 'FFF',
	'default-image' => get_template_directory_uri() . '/img/bg.jpg'
    ));*/

    // Add Support for Custom Header - Uncomment below if you're going to use
    /*add_theme_support('custom-header', array(
	'default-image'			=> get_template_directory_uri() . '/img/headers/default.jpg',
	'header-text'			=> false,
	'default-text-color'		=> '000',
	'width'				=> 1000,
	'height'			=> 198,
	'random-default'		=> false,
	'wp-head-callback'		=> $wphead_cb,
	'admin-head-callback'		=> $adminhead_cb,
	'admin-preview-callback'	=> $adminpreview_cb
    ));*/

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('html5blank', get_template_directory() . '/languages');
}

/*------------------------------------*\
	Functions
\*------------------------------------*/

// Register Custom Navigation Walker
require_once('wp_bootstrap_navwalker.php');

// HTML5 Blank navigation
function html5blank_nav()
{
	wp_nav_menu(
	array(
		'menu'              => 'primary',
		'theme_location'    => 'primary',
		'depth'             => 3,
		'container'         => 'div',
		'container_class'   => 'collapse navbar-collapse',
        'container_id'      => 'bs-example-navbar-collapse-1',
		'menu_class'        => 'nav navbar-nav navbar-justified',
		'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
		'walker'            => new wp_bootstrap_navwalker())
		
	);
}

register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'Base_Prag' ),
) );

// Load HTML5 Blank scripts (header.php)
function html5blank_header_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

    	
        
		wp_register_script('production', get_template_directory_uri() . '/js/build/production.min.js', array('jquery'), '1.0.0'); // Minified Production, all js in this file
		 wp_enqueue_script('production'); // Enqueue it!
		 
		 

        wp_register_script('html5blankscripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0.0'); // Custom scripts
        wp_enqueue_script('html5blankscripts'); // Enqueue it!
		
		
		
		

		
		wp_register_script('bootstrap', get_template_directory_uri() . '/node_modules/bootstrap/dist/js/bootstrap.min.js', array(), ''); // Bootstrap JS
		wp_enqueue_script('bootstrap'); // Enqueue it!
		}
		
		
		
}

// Ajax Pagination 

add_action( 'wp_ajax_nopriv_ajax_pagination', 'my_ajax_pagination' );
add_action( 'wp_ajax_ajax_pagination', 'my_ajax_pagination' );
function my_ajax_pagination() {
	$query_vars = json_decode( stripslashes( $_POST['query_vars'] ), true );
	$query_vars['paged'] = $_POST['page'];
	$posts = new WP_Query( $query_vars );
	$GLOBALS['wp_query'] = $posts;
	add_filter( 'editor_max_image_size', 'my_image_size_override' );
	if( ! $posts->have_posts() ) {
		get_template_part( 'content', 'none' );
	}
	else {
		while ( $posts->have_posts() ) {
			$posts->the_post();

			echo get_template_part('loop_basics');
		}
	}
	echo '<hr/>';
	remove_filter( 'editor_max_image_size', 'my_image_size_override' );
	the_posts_pagination( array(
'prev_text' => __( '', 'twentyfifteen' ),
'next_text' => __( '', 'twentyfifteen' ),
'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( '', 'twentyfifteen' ) . ' </span>',
) );
	wp_die();
}
function my_image_size_override() {
return array( 825, 510 );
}

// Load HTML5 Blank conditional scripts
function html5blank_conditional_scripts()
{
    if (is_page('pagenamehere')) {
        wp_register_script('scriptname', get_template_directory_uri() . '/js/scriptname.js', array('jquery'), '1.0.0'); // Conditional script(s)
        wp_enqueue_script('scriptname'); // Enqueue it!
    }
}

// Load HTML5 Blank styles
function html5blank_styles()
{

    wp_register_style('normalize', get_template_directory_uri() . '/normalize.css', array(), '1.0', 'all');
    wp_enqueue_style('normalize'); // Enqueue it!
	
	wp_register_style('bootstrap', get_template_directory_uri() . '/node_modules/bootstrap/dist/css/bootstrap.min.css', array(), ''); // Bootstrap JS
	wp_enqueue_style('bootstrap'); // Enqueue it!
	wp_register_style('html5blank', get_template_directory_uri() . '/css/output.css', array(), '1.0', 'all');
    wp_enqueue_style('html5blank'); // Enqueue it!
	


  
	
	
}

// Register HTML5 Blank Navigation
function register_html5_menu()
{
    register_nav_menus(array( // Using array to specify more menus if needed
        'header-menu' => __('Header Menu', 'html5blank'), // Main Navigation
        'sidebar-menu' => __('Sidebar Menu', 'html5blank'), // Sidebar Navigation
        'extra-menu' => __('Extra Menu', 'html5blank') // Extra Navigation if needed (duplicate as many as you need!)
    ));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
    // Define Sidebar Widget Area 1
    register_sidebar(array(
        'name' => __('Widget Area 1', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-1',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

    // Define Sidebar Widget Area 2
    register_sidebar(array(
        'name' => __('Right', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'right',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
}

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
    return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
    return 40;
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

// Custom View Article link to Post
function html5_blank_view_article($more)
{
    global $post;
    return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article', 'html5blank') . '</a>';
}

// Remove Admin bar
function remove_admin_bar()
{
    return false;
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// Custom Gravatar in Settings > Discussion
function html5blankgravatar ($avatar_defaults)
{
    $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
    $avatar_defaults[$myavatar] = "Custom Gravatar";
    return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}

// Custom Comments Callback
function html5blankcomments($comment, $args, $depth)
{
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
	<div class="comment-author vcard">
	<?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['180'] ); ?>
	<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
	</div>
<?php if ($comment->comment_approved == '0') : ?>
	<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
	<br />
<?php endif; ?>

	<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
		<?php
			printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
		?>
	</div>

	<?php comment_text() ?>

	<div class="reply">
	<?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	</div>
	<?php if ( 'div' != $args['style'] ) : ?>
	</div>
	<?php endif; ?>
<?php }

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('init', 'html5blank_header_scripts'); // Add Custom Scripts to wp_head
add_action('wp_print_scripts', 'html5blank_conditional_scripts'); // Add Conditional Page Scripts
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'html5blank_styles'); // Add Theme Stylesheet
add_action('init', 'register_html5_menu'); // Add HTML5 Blank Menu
add_action('init', 'create_post_type_html5'); // Add our HTML5 Blank Custom Post Type
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('avatar_defaults', 'html5blankgravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

// Shortcodes
add_shortcode('html5_shortcode_demo', 'html5_shortcode_demo'); // You can place [html5_shortcode_demo] in Pages, Posts now.
add_shortcode('html5_shortcode_demo_2', 'html5_shortcode_demo_2'); // Place [html5_shortcode_demo_2] in Pages, Posts now.

// Shortcodes above would be nested like this -
// [html5_shortcode_demo] [html5_shortcode_demo_2] Here's the page title! [/html5_shortcode_demo_2] [/html5_shortcode_demo]

/*------------------------------------*\
	Custom Post Types
\*------------------------------------*/

// Create 1 Custom Post type for a Demo, called HTML5-Blank
function create_post_type_html5()
{
    register_taxonomy_for_object_type('category', 'html5-blank'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'html5-blank');
    register_post_type('html5-blank', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('HTML5 Blank Custom Post', 'html5blank'), // Rename these to suit
            'singular_name' => __('HTML5 Blank Custom Post', 'html5blank'),
            'add_new' => __('Add New', 'html5blank'),
            'add_new_item' => __('Add New HTML5 Blank Custom Post', 'html5blank'),
            'edit' => __('Edit', 'html5blank'),
            'edit_item' => __('Edit HTML5 Blank Custom Post', 'html5blank'),
            'new_item' => __('New HTML5 Blank Custom Post', 'html5blank'),
            'view' => __('View HTML5 Blank Custom Post', 'html5blank'),
            'view_item' => __('View HTML5 Blank Custom Post', 'html5blank'),
            'search_items' => __('Search HTML5 Blank Custom Post', 'html5blank'),
            'not_found' => __('No HTML5 Blank Custom Posts found', 'html5blank'),
            'not_found_in_trash' => __('No HTML5 Blank Custom Posts found in Trash', 'html5blank')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
            'post_tag',
            'category'
        ) // Add Category and Post Tags support
    ));
}


/*------------------------------------*\
	Custom Columns
\*------------------------------------*/

// ADD NEW COLUMN
function v2008_c_head($defaults) {
	$column_name = 'profile_type';//column slug
	$column_heading = 'profile_type';//column heading
	$defaults[$column_name] = $column_heading;
	return $defaults;
}
 
// SHOW THE COLUMN CONTENT
function v2008_c_content($name, $post_ID) {
    $column_name = 'profile_type';//column slug	
    $column_field = 'profile_type';//field slug	
    if ($name == $column_name) {
        $post_meta = get_post_meta($post_ID,$column_field,true);
        if ($post_meta) {
            echo $post_meta;
        }
    }
}

// ADD STYLING FOR COLUMN
function v2008_c_style(){
	$column_name = 'profile_type';//column slug	
	echo "<style>.column-$column_name{width:10%;}</style>";
}

add_filter('manage_solutions_posts_columns', 'v2008_c_head');
add_action('manage_solutions_posts_custom_column', 'v2008_c_content', 10, 2);
add_filter('admin_head', 'v2008_c_style');

/*------------------------------------*\
	ShortCode Functions
\*------------------------------------*/

// Shortcode Demo with Nested Capability
function html5_shortcode_demo($atts, $content = null)
{
    return '<div class="shortcode-demo">' . do_shortcode($content) . '</div>'; // do_shortcode allows for nested Shortcodes
}

// Shortcode Demo with simple <h2> tag
function html5_shortcode_demo_2($atts, $content = null) // Demo Heading H2 shortcode, allows for nesting within above element. Fully expandable.
{
    return '<h2>' . $content . '</h2>';
}

//[foobar]
function foobar_func( $atts ){
	return "foo and bar";
	
}
add_shortcode( 'foobar', 'foobar_func' );

/**
 *  wp_query shortcode Shortcode    [wp-post showpost="2" post_type="post" pagination="true" include_excerpt='true' ]
 */
 
 
 
 
function wp_query_shortcode($atts, $content = null)
        { 
        extract(shortcode_atts(array(
                'showpost' => '-1',
				'category' => '',
                'post_type' => 'post',
                'pagination' => '',
				'tags' => '',
                'include_excerpt' => ''
        ) , $atts));
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $args = array(
                'post_type' => $post_type,
				'tag' => $tags,
                'paged' => $paged,
				'category' => $category,
                'order' => 'ASC',
                'orderby' => 'date',
				'eventDisplay' => 'past',
                'posts_per_page' => $showpost,
        );
        $query = new WP_Query($args);
        if ($query->have_posts())
			$content.= '<article class="results_table">';
			 $content.= '<span class="results_row">';
			$content.= '<span class="results_cell"><strong>Start Date</strong></span>';
			$content.= '<span class="results_cell"><strong>End Date</strong></span>';
			$content.= '<span class="results_cell"><strong>Title</strong></span>';
			$content.= '<span class="results_cell"><strong>Venue</strong></span>';
			$content.= '<span class="results_cell"><strong>Series</strong></span>';
			$content.= '</span>';
			$content.= '<hr class="clearfix"/>';
        while ($query->have_posts()):
                $query->the_post();
                $content.= '<span class="results_row">';
				$content.= '<span class="results_cell">' . tribe_get_start_date() . '</span>';
				$content.= '<span class="results_cell">' . tribe_get_end_date() . '</span>';
                $content.= '<a class="results_cell" href=' . get_permalink() . '>' . get_the_title() . '</a>';
			    $content.= '<span class="results_cell">' . tribe_get_venue() . '</span>';
				$content.= get_field('series');
				
                if ($include_excerpt == 'true')
                        {
                        $content.= '<div class="entry-content">' . do_shortcode(get_the_excerpt()) . '</div>';
                        }
                  else
                        {
                        $content.= '<div class="entry-content"></div>';
                        }
				
                 $content.= '</span>';
				 $content.= '<hr class="clearfix"/>';
				
        endwhile; // end of the loop.
		
        /* Pagination  Code Start  */
        if ($pagination == 'true'):
                global $wp_query;
                $big = 999999999; // need an unlikely integer
                $totalpages = $query->max_num_pages;
                $current = max(1, get_query_var('paged'));
                $paginate_args = array(
                        'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))) ,
                        'format' => '?paged=%#%',
                        'current' => $current,
                        'total' => $totalpages,
                        'show_all' => False,
                        'end_size' => 1,
                        'mid_size' => 3,
                        'prev_next' => True,
                        'prev_text' => __('« Previous') ,
                        'next_text' => __('Next »') ,
                        'type' => 'plain',
                        'add_args' => False,
                        'add_fragment' => '',
                        'before_page_number' => '',
                        'after_page_number' => ''
                );
                $pagination = paginate_links($paginate_args);
                $content.= "<nav class='pagination'>";
                $content.= "<span class='page-numbers page-no'>Page " . $paged . " of " . $totalpages . "</span> ";
                $content.= $pagination;
                $content.= "</nav>";
        endif;
        wp_reset_postdata();
		
        return $content;
        }
 
add_shortcode('wp-post', 'wp_query_shortcode');

/**
 *  wp_query profile shortcode Shortcode    [wp-profiles showpost="2" post_type="profile" pagination="true" include_excerpt='true' ]
 */
 

function wp_profiles_shortcode($atts, $content = null)
        { 
        extract(shortcode_atts(array(
                'showpost' => '-1',
                'post_type' => 'profile',
                'pagination' => '',
				'tags' => '',
                'include_excerpt' => '',
				'profile_name' => '',
				'profile_type' => ''
        ) , $atts));
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        if ($profile_name == ''){
		$args = array(
                'post_type' => $post_type,
				'tag' => $tags,
                'paged' => $paged,
                'order' => 'ASC',
                'orderby' => 'date',
                'posts_per_page' => $showpost,
				'profile_type' => $profile_type,
				'meta_key'		=> 'profile_type',
				'meta_value'	=> $profile_type
				);	
		
		} else { 
		
		$args = array(
                'post_type' => $post_type,
				'tag' => $tags,
                'paged' => $paged,
                'order' => 'ASC',
				'orderby' => 'date',
                'posts_per_page' => $showpost,
				'meta_query'	=> array(
				'relation'		=> 'AND',
				array(
					'key'		=> 'profile_name',
					'value'		=> $profile_name,	
					'compare'	=> '='
				),
				array(
					'key'		=> 'profile_type',
					'value'		=> $profile_type,	
					'compare'	=> '='
				),
                
				)
		);
		}
        $query = new WP_Query($args);
        if ($query->have_posts())
			
			 $content.= '<div class="results_table">';
        while ($query->have_posts()):
                $query->the_post();
                if ( get_field('profile_image') ){
				$content.= '<div class="profile_item col-xs-6 col-sm-4 text-center"><div class="profile_thumbnail bg_contain" style="background-image:url(&quot;'. get_field('profile_image') .' &quot; )"></div>';
				} else {
				$content.= '<div class="profile_item col-xs-6 col-sm-4 text-center"><div class="profile_thumbnail bg_contain" style="background-image:url(&quot;'. get_field('gallery_image_placeholder', 35) .' &quot; )"></div>';	
				};
				$content.= '';
				$content.= '<span class="text-center"><a href=' . get_permalink() . '>' . get_field('profile_name') . '</a></span><br/>' . get_field('profile_meta') . ' </div>';
                
				
                if ($include_excerpt == 'true')
                        {
                        $content.= '<div class="entry-content">' . do_shortcode(get_the_excerpt()) . '</div>';
                        }
                  else
                        {
                        $content.= '';
                        }
				
                
				
				
        endwhile; // end of the loop.
		$content.= '</div>';
        /* Pagination  Code Start  */
        if ($pagination == 'true'):
                global $wp_query;
                $big = 999999999; // need an unlikely integer
                $totalpages = $query->max_num_pages;
                $current = max(1, get_query_var('paged'));
                $paginate_args = array(
                        'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))) ,
                        'format' => '?paged=%#%',
                        'current' => $current,
                        'total' => $totalpages,
                        'show_all' => False,
                        'end_size' => 1,
                        'mid_size' => 3,
                        'prev_next' => True,
                        'prev_text' => __('« Previous') ,
                        'next_text' => __('Next »') ,
                        'type' => 'plain',
                        'add_args' => False,
                        'add_fragment' => '',
                        'before_page_number' => '',
                        'after_page_number' => ''
                );
                $pagination = paginate_links($paginate_args);
                $content.= "<nav class='pagination'>";
                $content.= "<span class='page-numbers page-no'>Page " . $paged . " of " . $totalpages . "</span> ";
                $content.= $pagination;
                $content.= "</nav>";
        endif;
        wp_reset_postdata();
		
        return $content;
        }
 
add_shortcode('wp-profiles', 'wp_profiles_shortcode');

/**
 *  wp_query shortcode Shortcode    [wp-userpost showpost="2" post_type="post" pagination="true" include_excerpt='true' category_name="lost-and-found"]
 */
 
 
 
 
function wp_userpost_shortcode($atts, $content = null)
        { 
		wp_reset_postdata();
        extract(shortcode_atts(array(
                'showpost' => '-1',
				'category' => '',
                'post_type' => 'post',
                'pagination' => '',
				'category_name' => '',
				'tags' => '',
                'include_excerpt' => ''
        ) , $atts));
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $args = array(
                'post_type' => $post_type,
                'paged' => $paged,
				'category_name' => $category_name,
                'order' => 'ASC',
                'orderby' => 'date',
                'posts_per_page' => $showpost,
        );
        $query = new WP_Query($args);
        if ($query->have_posts())
			$content.= '<article class="results_table">';
			 $content.= '<span class="results_row">';
			$content.= '<span class="results_cell"><strong>Category</strong></span>';
			$content.= '<span class="results_cell"><strong>Date</strong></span>';
			$content.= '<span class="results_cell"><strong>Title</strong></span>';
			
			$content.= '</span>';
			$content.= '<hr class="clearfix"/>';
        while ($query->have_posts()):
                $query->the_post();
                $content.= '<span class="results_row">';
				$content.= '<span class="results_cell">' . get_the_category_list() . '</span>';
				$content.= '<span class="results_cell">' . 
				get_the_date(). '</span>';
                $content.= '<a class="results_cell" href=' . get_permalink() . '>' . get_the_title() . '</a>';
			   
				
				
                
				
                 $content.= '</span>';
				 $content.= '<hr class="clearfix"/>';
				
        endwhile; // end of the loop.
		
        /* Pagination  Code Start  */
        if ($pagination == 'true'):
                global $wp_query;
                $big = 999999999; // need an unlikely integer
                $totalpages = $query->max_num_pages;
                $current = max(1, get_query_var('paged'));
                $paginate_args = array(
                        'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))) ,
                        'format' => '?paged=%#%',
                        'current' => $current,
                        'total' => $totalpages,
                        'show_all' => False,
                        'end_size' => 1,
                        'mid_size' => 3,
                        'prev_next' => True,
                        'prev_text' => __('« Previous') ,
                        'next_text' => __('Next »') ,
                        'type' => 'plain',
                        'add_args' => False,
                        'add_fragment' => '',
                        'before_page_number' => '',
                        'after_page_number' => ''
                );
                $pagination = paginate_links($paginate_args);
                $content.= "<nav class='pagination'>";
                $content.= "<span class='page-numbers page-no'>Page " . $paged . " of " . $totalpages . "</span> ";
                $content.= $pagination;
                $content.= "</nav>";
        endif;
        wp_reset_postdata();
		
        return $content;
        }
 
add_shortcode('wp-userpost', 'wp_userpost_shortcode');
?>
