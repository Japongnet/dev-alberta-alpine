<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<!-- Beginning of Header.php -->
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

	</head>
	
	<!-- Check to see if there's a featured background image or if you want the random slideshow -->
	
		<body>
	
		
	<?php if(get_field('background_image')){ ?>	
		<div class="body_bg bg_cover"  style="background-image:url('<?php the_field('background_image'); ?>')">
		</div> 
	<?php } else { ?>
		<div class="body_bg bg_cover"  style="background-image:url('<?php the_field('background_image', 35); ?>')">
		</div>
	<?php } ?>

		<!-- wrapper -->
		<div class="wrapper">

			<div class="header_block">
				<div class="promo_slider">
				<?php
					query_posts( array( 'post_type' => 'ad' ) );
					if ( have_posts() ) : while ( have_posts() ) : the_post();
				?>
					<a class="promo_slide" href="<?php the_field('ad_link'); ?>" target="_blank"><div class="ad_slide_image bg_contain" style="background-image:url('<?php the_field('ad_image'); ?>')"></div>
					</a>
					
					<?php endwhile; endif; wp_reset_query(); ?>
					
				</div>
			</div>
			<!-- header -->
			<header class="header clear" role="banner">

					<!-- logo -->
					<div class="header_wrapper">
						<div class="header_gradient_bottom">
							 
							</div>
							<div class="header_gradient_top">
							
							</div>
						<div class="logo col-xs-12 col-sm-3  header_section">
							<a href="<?php echo home_url(); ?>">
								<!-- svg logo - toddmotto.com/mastering-svg-use-for-a-retina-web-fallbacks-with-png-script -->
								<img src="<?php echo get_template_directory_uri(); ?>/img/AA_logo.png" alt="Logo" class="logo-img hidden-xs">
								<br/>
								<img src="<?php echo get_template_directory_uri(); ?>/img/Alberta_Alpine_wide.png" alt="Logo" class="logo-img hidden-sm hidden-md hidden-lg padding_general">
							</a>
						</div>
						<!-- /logo -->
						<div class="header_section col-xs-12 col-sm-9">
							
						
						
						
							<div class="slider_container col-xs-12 pull-left">
									<div class="slider_tabs">
										<div class="slider_triangle_left">
										</div>
										<div class="slider_tab sidebar_background">
											<a href="<?php the_field('tab_link_01', 35); ?>"><?php the_field('tab_text_01', 35); ?></a>
										</div>
										<div class="slider_triangle">
										</div>
										<div class="slider_tab main_background">
											<a href="<?php the_field('tab_link_02', 35); ?>"><?php the_field('tab_text_02', 35); ?></a>
										</div>
									</div>
								<div class="slider"> <!-- Call the 'slide' post type, loop through the custom fields to populate the slides -->
									
									<?php
									query_posts( array( 'post_type' => 'slide' ) );
									if ( have_posts() ) : while ( have_posts() ) : the_post();
									?>
										<div class="slide bg_cover" style="background-image:url('<?php the_field('slide_image'); ?>')">	
											<a href="<?php the_field('slide_link'); ?>" target="_blank">
											
											<h3 class="slab slide_headline pull-right"><?php the_field('slide_headline'); ?></h3></a>
											<?php if(get_field('slide_description')){ ?> 
											<div class="slide_description">
										
												<span class="text-right col-xs-12 no-padding"><?php the_field('slide_description'); ?></span>
											</div>
											<?php } ?>
											
										</div>
							<?php endwhile; endif; wp_reset_query(); ?>
									
									
									
								</div>
							</div>
						</div>
					</div>

					<!-- nav -->
					<nav class="nav nav-justified table_container collapse navbar-collapse" >
						<?php html5blank_nav(); ?>
						<span class="end_cap">
							<span class="nav_square">
							</span>
							<span class="nav_triangle">
							</span>	
						</span>
					</nav>
					 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar main_background"></span>
						<span class="icon-bar main_background"></span>
						<span class="icon-bar main_background"></span>
					  </button>
					
					<!-- /nav -->

			</header>
			<!-- /header -->
			<div class="mid-wrapper"> <!-- Ends in Footer -->
				<div class="col-xs-12 visible-sm-* visible-md-* visible-lg-* col-sm-3  block-section mid-section">
					
					<?php get_sidebar(); ?>
				</div>
				<div role="main" class="main col-xs-12 col-sm-6 block-section  mid-section">
				<!-- section -->
				<section> <!-- Ends in Footer -->
			<!-- End of Header.php -->