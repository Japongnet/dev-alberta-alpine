..<?php get_header(); ?>
			<!-- beginning of Index.php -->
			
			
			
			<?php 
			// the query
			$the_query = new WP_Query( 'category_name=featured&posts_per_page=1&orderby=date&order=DESC' ); ?>

			<?php if ( $the_query->have_posts() ) : ?>
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
			<div class="hidden-md hidden-lg">
				<h2 class="sidebar_foreground">NEWS</h2>
			</div>
			<div class="news_section_header clearfix">
				<div class="news_section_overlay hidden-xs hidden-sm">
					<div class="news_section_left">
						<div class="news_section_upper">
							<div class="news_section_upper_triangle transparency">
							
							</div>
							<div class="news_section_upper_box sidebar_background transparency">
							
							</div>
						</div>
						<div class="news_section_lower_box sidebar_background transparency">
						<h2 class="news_section_title">NEWS</h2>
						</div>
						
					</div>
					<div class="news_section_big_triangle transparency">
					
					</div>
				</div>
				<?php if (has_post_thumbnail( $post->ID ) ): ?>
<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>

<?php endif; ?>
				<div class="news_section_header_image bg_cover" style="background-image:url('<?php echo $image[0]; ?>')">
					
				</div>
				
			</div>
			<div class="news_section_header_text">
				<a href="<?php the_permalink(); ?>" class="sidebar_foreground"><h3><?php the_title(); ?></h3></a>
				<?php the_excerpt() ?>
			</div>
			<hr/>
			<?php endwhile; ?>
			<?php else : ?>
				<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>

			
			<?php wp_reset_postdata(); ?>
			
			
						
			
			<?php 
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			// the query
			$cat_id1 = get_cat_ID('Lost and Found');
			$cat_id2 = get_cat_ID('Buy and Sell');
			$the_query = new WP_Query( 'paged=' . $paged.'&orderby=date&order=DESC&cat=-'.$cat_id1.',-'.$cat_id2.'' ); ?>

			<?php if ( $the_query->have_posts() ) : ?>
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
			<div class="hidden-md hidden-lg">
				<h2 class="sidebar_foreground">NEWS</h2>
			</div>
			<?php get_template_part('loop_basics'); ?>
			<?php endwhile; ?>
			<?php else : ?>
				<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>

			
			
		
						<?php the_posts_pagination( array(
'prev_text' => __( '', 'twentyfifteen' ),
'next_text' => __( '', 'twentyfifteen' ),
'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( '', 'twentyfifteen' ) . ' </span>',
) ); ?>
		
    <!-- End of Index.php -->

<?php get_footer(); ?>
