<!-- sidebar.php -->
<aside class="sidebar" role="complementary">

	

	<div class="sidebar-widget">
		<div class="social_icons text-justify">
			<a href="<?php the_field('twitter_link', 35); ?>" class="sidebar_foreground" target="_blank"><i class="fa fa-twitter"></i></a>
			<a href="<?php the_field('facebook_link', 35); ?>" class="sidebar_foreground" target="_blank"><i class="fa fa-facebook"></i></a>
			<a href="<?php the_field('mail_link', 35); ?>" class="sidebar_foreground" target="_blank"><i class="fa fa-envelope"></i></a>
			<a href="<?php the_field('youtube_link', 35); ?>"  class="sidebar_foreground" target="_blank"><i class="fa fa-youtube-play"></i></a>
		</div>
		
		
		<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('widget-area-1')) ?>
	</div>

	<div class="sidebar-widget">
		<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('widget-area-2')) ?>
		
	</div>

</aside>
<!-- /sidebar.php -->
